<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<?php
    
    require 'database.php';
    
    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
		$devError = null;
        $imageError = null;
		$promoImageError = null;
		$text_promoError = null;
		$linkError = null;
		
        //$allowedExts = array("jpg", "jpeg", "gif", "png");
        
        // keep track post values
		$name = $_POST['name'];
		$dev = $_POST['dev'];
        $image = file_get_contents($_FILES['image']['tmp_name']);		
		$promo_text = $_POST['promo_text'];
		$promoImage = file_get_contents($_FILES['promo_image']['tmp_name']);	
	    $link = $_POST['link'];
	
        
        // validate input
        $valid = true;
       
		if (empty($name)) {
            $textError = 's\'il vous plaît entrer nome de l\'app';
            $valid = false;
        } //end if
		
		if (empty($dev)) {
            $textError = 's\'il vous plaît entrer developer';
            $valid = false;
        } //end if
		
		if (empty($image)) {
            $imageError = 's\'il vous plaît entrer la icon';
            $valid = false;
        } //end if
        
	    if (empty($promo_text)) {
            $textError = 's\'il vous plaît entrer texte promo';
            $valid = false;
        } //end if
		
		if (empty($promoImage)) {
            $promoImageError = 's\'il vous plaît entrer la image promotionel';
            $valid = false;
        } //end if
		
		if (empty($link)) {
            $textError = 's\'il vous plaît entrer lien d\'app';
            $valid = false;
        } //end if
			
		// insert data
        if ($valid) {			
					
		   $pdo = Database::connect();
           $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           $sql = "INSERT INTO promoapp (name, dev, image, promo_text, promo_image, link) values(?, ?, ?, ?, ?, ?)";
           $q = $pdo->prepare($sql);
           $q->execute(array($name, $dev, $image, $promo_text, $promoImage, $link));
           Database::disconnect();
           header("Location: index.php");
            
        } // end if
            
    } // end if
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link   href="css/bootstrap.min.css" rel="stylesheet">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
<header class="header" style="text-align:center;">
<img src="img/logo.png" width="133" height="133" style="margin-top:20px;">
</header>

<div class="span10 offset1">
<div class="row">
<h3>Creer App</h3>
</div>

<form enctype="multipart/form-data" class="form-horizontal" action="create_apppremium.php" method="post">

<div class="control-group <?php echo !empty($nameError)?'error':'';?>">
<label class="control-label">Nom de l'application Premium</label>
<div class="controls">
 <input name="name" type="text" placeholder="Nom d'application" value="<?php echo !empty($name)?$name:'';?>">
 <?php if (!empty($nameError)): ?>
 <span class="help-inline"><?php echo $nameError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="control-group <?php echo !empty($devError)?'error':'';?>">
<label class="control-label">Nom Developer</label>
<div class="controls">
 <input name="dev" type="text" placeholder="Nom developer" value="<?php echo !empty($dev)?$dev:'';?>">
 <?php if (!empty($devError)): ?>
 <span class="help-inline"><?php echo $devError;?></span>
 <?php endif; ?>
</div>
</div>


<div class="control-group">
<label class="control-label">Icon</label>
<div class="controls">
<input name="image" type="file" id="image"> 

</div>
</div>

<div class="control-group <?php echo !empty($promo_textError)?'error':'';?>">
<label class="control-label">Texte Promotional</label>
<div class="controls">
 <textarea name="promo_text" placeholder="Texte promo" cols="80" rows="6"><?php echo !empty($promo_text)?$promo_text:'';?></textarea>
 <?php if (!empty($promo_textError)): ?>
 <span class="help-inline"><?php echo $promo_textError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="control-group">
<label class="control-label">Image promo</label>
<div class="controls">
<input name="promo_image" type="file" id="promo_image"> 

</div>
</div>

<div class="control-group <?php echo !empty($linkError)?'error':'';?>">
<label class="control-label">Lien</label>
<div class="controls">
 <input name="link" type="text" placeholder="Link d'application" value="<?php echo !empty($link)?$link:'';?>">
 <?php if (!empty($linkError)): ?>
 <span class="help-inline"><?php echo $linkError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="form-actions">
<button type="submit" class="btn btn-success">Envoyer</button>
<a class="btn" href="index.php">Arrière</a>
</div>
</form>
</div>

</div> <!-- /container -->
</body>
</html>