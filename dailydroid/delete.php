<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<?php
    require 'database.php';
    $id = null;
    
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
    
    if ( null==$id ) {
        header("Location: index.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM DailyDroidApp where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
    
    
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
        
        // delete data
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM DailyDroidApp WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::disconnect();
        header("Location: index.php");
        
    }
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link   href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
<header class="header" style="text-align:center;">
<img src="img/logo.png" width="133" height="133" style="margin-top:20px;">
</header>

<div class="span10 offset1">
<div class="row">
<h3>Effacer App</h3>
</div>

<form class="form-horizontal" action="delete.php" method="post">
<input type="hidden" name="id" value="<?php echo $id;?>"/>
<p class="alert alert-error">Effacer cet App?</p>
<div class="form-horizontal" >

<div class="control-group">
<label class="control-label">Nom d'application</label>
<div class="controls">
<label class="checkbox">
<?php echo $data['name'];?>
</label>
</div>
</div>

<div class="control-group">
<label class="control-label">Nom Developer</label>
<div class="controls">
<label class="checkbox">
<?php echo $data['name'];?>
</label>
</div>
</div>


<div class="control-group">
<label class="control-label">Icon</label>
<div class="controls">
<label class="checkbox">
<?php echo '<img src="display_img.php?id='.$data['id'].'" width="100" height="100">';?>
</label>
</div>
</div>

<div class="form-actions">
<button type="submit" class="btn btn-danger">Oui</button>
<a class="btn" href="index.php">Non</a>
</div>
</form>
</div>

</div> <!-- /container -->
</body>
</html>