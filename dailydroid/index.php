<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link   href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/bootstrap.min.js"></script>
<style>
#table-apps h1{
   font-size:16px;
   margin-top:6px;
   margin-bottom:3px;
}
.container{
   margin-bottom:190px;
}
</style>
</head>

<body>
<div class="container">
<header class="header" style="text-align:center;">
   <img src="img/logo.png" width="133" height="133" style="margin-top:20px;">
</header>
<div class="row">
<h6>Hello <?php echo htmlentities($_SESSION['user']['username'], ENT_QUOTES, 'UTF-8'); ?></h6>
<h3>DailyDroid - Apps</h3>
</div>
<div class="row">

<p style="float:left; margin-right:15px;">
<a href="create_apppremium.php" class="btn btn-success">Créer App Premium</a>
</p>
<p style="float:left; margin-right:15px;">
<a href="push/index.php" class="btn btn-success">Push notification</a>
</p>
<p style="float:left; margin-right:15px;">
<a href="logout.php" class="btn btn-danger">Logout</a>
</p>

<!-- Promo App -->
<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Nom de l'application Premium</th>
<th>Dev</th>
<th>Icon</th>
<th>Texte Promotional</th>
<th>Promo image</th>
<th>Lien</th>
<th>Action</th>
</tr>
</thead>
<tbody id="table-apps">
<?php
    include 'database.php';
    $pdo = Database::connect();
    $sql = 'SELECT * FROM promoapp ORDER BY id DESC';	
    foreach ($pdo->query($sql) as $row) {
        echo '<tr>';
		echo '<td>'. $row['name'] . '</td>';
		echo '<td>'. $row['dev'] . '</td>';
        echo '<td>' . '<img src="display_imgpremium.php?id='.$row['id'].'" width="100" height="100">' . '</td>';
        echo '<td width=950 style="width:950px !important;">'. $row['promo_text'] . '</td>';
		echo '<td>' . '<img src="display_promoimgpremium.php?id='.$row['id'].'" width="100" height="auto">' . '</td>';
		echo '<td>'. $row['link'] . '</td>';
        echo '<td width=250>';
        echo ' ';
        echo '<a class="btn btn-success" style="margin-bottom:9px;" href="updatepremium.php?id='.$row['id'].'">Réviser</a>';
        echo '<br>';
        echo '<a class="btn btn-danger" href="deletepremium.php?id='.$row['id'].'">Effacer</a>';
        echo '</td>';
        echo '</tr>';
    }
    //Database::disconnect();
    ?>
</tbody>
</table>
<!-- Promo App -->

<p style="float:left; margin-right:15px;">
<a href="create_app.php" class="btn btn-success">Créer App</a>
</p>

<table class="table table-striped table-bordered">
<thead>
<tr>
<th>Nom de l'application</th>
<th>Dev</th>
<th>Icon</th>
<th>Texte Promotional</th>
<th>Promo image</th>
<th>Lien</th>
<!--<th>Prix</th>
<th>Lien pour social network</th>-->
<th>Action</th>
</tr>
</thead>
<tbody id="table-apps">
<?php
    //include 'database.php';
    //$pdo = Database::connect();
    $sql = 'SELECT * FROM DailyDroidApp ORDER BY id DESC';
    foreach ($pdo->query($sql) as $row) {
        echo '<tr>';
		echo '<td>'. $row['name'] . '</td>';
		echo '<td>'. $row['dev'] . '</td>';
        echo '<td>' . '<img src="display_img.php?id='.$row['id'].'" width="100" height="100">' . '</td>';
        echo '<td width=950 style="width:950px !important;">'. $row['promo_text'] . '</td>';
		echo '<td>' . '<img src="display_promoimg.php?id='.$row['id'].'" width="100" height="auto">' . '</td>';
		echo '<td>'. $row['link'] . '</td>';
		//echo '<td>'. $row['price'] . '</td>';
		//echo '<td width="20%">'. $row['linkToShare'] . '</td>';
        echo '<td width=250>';
        echo ' ';
        echo '<a class="btn btn-success" style="margin-bottom:9px;" href="update.php?id='.$row['id'].'">Réviser</a>';
        echo ' ';
        echo '<a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Effacer</a>';
        echo '</td>';
        echo '</tr>';
    }
    Database::disconnect();
    ?>
</tbody>
</table>
</div>
</div> <!-- /container -->
</body>
</html>