<?php 

    // First we execute our common code to connection to the database and start the session 
    require("../common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: ../login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to ../login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
			
			   $('#pushForm').submit(function(e){
	    		//var landmarkID = $(this).parent().attr('data-landmark-id');
	    		//var postData = $(this);
	    		e.preventDefault;
	    		e.stopPropagation;
	    		
	    		
	    		$.ajax({
	    			type: 'GET',
	    			url: 'http://freelanceando.net/dailydroid/push/send_message.php',
	    			data: $('#pushForm').serialize(),
	    			cache: false,
	    			dataType: 'html',
	    			success: function(data){
    				        console.log(data);	    				
	    					alert('success!'+data);	    			
	    		    },
					error: function(){
	    				//console.log(data);
	    				alert('error!'+data);
	    			}
	    		
	    	});
	    		
	        return false;	
	    });
                
            });
            
        </script>
        <style type="text/css">
            .container{
                width: 950px;
                margin: 100px auto;
                padding: 0;
            }
            h1{
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                font-size: 24px;
                color: #777;
            }
            div.clear{
                clear: both;
            }
            ul.devices{
                margin: 0;
                padding: 0;				
            }
            ul.devices li{
                float: left;
                list-style: none;
                border: 1px solid #dedede;
                padding: 10px;
                margin: 0 15px 25px 0;
                border-radius: 3px;
                -webkit-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
                -moz-box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
                box-shadow: 0 1px 5px rgba(0, 0, 0, 0.35);
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                color: #555;
				
            }
            ul.devices li label, ul.devices li span{
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
                font-size: 12px;
                font-style: normal;
                font-variant: normal;
                font-weight: bold;
                color: #393939;
                display: block;
                float: left;
            }
            ul.devices li label{
                height: 25px;
                width: 50px;                
            }
            ul.devices li textarea{
                float: left;
                resize: none;
				width:500px;
				height:100px;
            }
			#title{
			    width:300px;
				margin-bottom:14px;
			}
            ul.devices li .send_btn{
                background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
                background: -webkit-linear-gradient(0% 0%, 0% 100%, from(#0096FF), to(#005DFF));
                background: -moz-linear-gradient(center top, #0096FF, #005DFF);
                background: linear-gradient(#0096FF, #005DFF);
                text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3);
                border-radius: 3px;
                color: #fff;
            }
        </style>
    </head>
    <body>
        
        <div class="container">
            <h1>Envoyer Un Push Notification</h1>
            <hr/>
            <ul class="devices">
               
                        <li>
                            <form id="pushForm" method="post">                                
                                <div class="clear"></div>
                                <div class="send_container">        
								    <input type="text" id="title" name="title" placeholder="Title"><br>
                                    <textarea rows="3" id="message" name="message" cols="25" class="txt_message" placeholder="Message ici"></textarea>
                                    <input type="submit" class="send_btn" value="Send" onclick=""/>
                                </div>
                            </form>
                        </li>
                    
               
            </ul>
        </div>
    </body>
</html>