<?php 

    // First we execute our common code to connection to the database and start the session 
    require("../common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: ../login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to ../login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<?php

$title = $_GET['title'];
$message = $_GET['message'];

// check data base
include_once './db_functions.php';
        $db = new DB_Functions();
        $users = $db->getAllUsers();
        if ($users != false)
            $no_of_users = mysql_num_rows($users);
        else
            $no_of_users = 0;
		
while ($row = mysql_fetch_array($users)) {		

    //if (isset($_REQUEST["device"]) && isset($_REQUEST["message"])) {
    $device = $row["device"];     
	$showstatusbar = "3";
	
    include_once './GCM.php';
     
    $gcm = new GCM();
    
	if($row["notify"] == 0){
      $registration_ids = array($device);	  
      $messagee = array("message" => $message, "title" => $title, "msgcnt" => $showstatusbar); 
      $result = $gcm->send_notification($registration_ids, $messagee);    
      echo $result;
    }

}
?>