<?php 

    // First we execute our common code to connection to the database and start the session 
    require("common.php"); 
     
    // At the top of the page we check to see whether the user is logged in or not 
    if(empty($_SESSION['user'])) 
    { 
        // If they are not, we redirect them to the login page. 
        header("Location: login.php"); 
         
        // Remember that this die statement is absolutely critical.  Without it, 
        // people can view your members-only content without logging in. 
        die("Redirecting to login.php"); 
    } 
     
    // Everything below this point in the file is secured by the login system 
     
    // We can display the user's username to them by reading it from the session array.  Remember that because 
    // a username is user submitted content we must use htmlentities on it before displaying it to the user. 
?>
<?php
    require 'database.php';
    
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
    
    if ( null==$id ) {
        header("Location: index.php");
    }
    
    if ( !empty($_POST)) {
        // keep track validation errors
        $nameError = null;
		$devError = null;
        $imageError = null;
		$promoImageError = null;
		$text_promoError = null;
		$linkError = null;
		//$priceError = null;
		//$linkToShareError = null;
        //$allowedExts = array("jpg", "jpeg", "gif", "png");
        
        // keep track post values
		$name = $_POST['name'];
		$dev = $_POST['dev'];
        $image = file_get_contents($_FILES['image']['tmp_name']);		
		$promo_text = $_POST['promo_text'];
		$promoImage = file_get_contents($_FILES['promo_image']['tmp_name']);	
	    $link = $_POST['link'];
		//$price = $_POST['price'];
		//$linkToShare = $_POST['linkToShare'];
        
        // validate input
        $valid = true;
       
		if (empty($dev)) {
            $textError = 's\'il vous plaît entrer developer';
            $valid = false;
        } //end if
		
		/*
		if (empty($image)) {
            $imageError = 's\'il vous plaît entrer la icon';
            $valid = false;
        } //end if */
        
	    if (empty($promo_text)) {
            $textError = 's\'il vous plaît entrer texte promo';
            $valid = false;
        } //end if
	    
		/*
		if (empty($promoImage)) {
            $promoImageError = 's\'il vous plaît entrer la image promotionel';
            $valid = false;
        } //end if*/
		
		if (empty($link)) {
            $textError = 's\'il vous plaît entrer lien d\'app';
            $valid = false;
        } //end if
		
        /*if (empty($price)) {
            $textError = 's\'il vous plaît entrer le prix';
            $valid = false;
        } //end if
		
		if (empty($linkToShare)) {
            $linkToShareError = 's\'il vous plaît entrer le lien pour le social network share';
            $valid = false;
        } //end if
        */
        // update data
        if ($valid) {
            $pdo = Database::connect();
			
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			if($image != '' && $promoImage == ''){
              $sql = "UPDATE DailyDroidApp  set name = ?, dev = ?, image = ?, promo_text = ?, link = ? WHERE id = ?";
			}elseif($image == '' && $promoImage != ''){
			  $sql = "UPDATE DailyDroidApp  set name = ?, dev = ?, promo_text = ?, promo_image = ?, link = ? WHERE id = ?";
			}elseif($image != '' && $promoImage != ''){
			  $sql = "UPDATE DailyDroidApp  set name = ?, dev = ?, image = ?, promo_text = ?, promo_image = ?, link = ? WHERE id = ?";
			}else{
			  $sql = "UPDATE DailyDroidApp  set name = ?, dev = ?, promo_text = ?, link = ? WHERE id = ?";
			}  
            $q = $pdo->prepare($sql);
			if($image != '' && $promoImage == ''){
              $q->execute(array($name, $dev, $image, $promo_text, $link, $id));
			}elseif($image == '' && $promoImage != ''){
			  $q->execute(array($name, $dev, $promo_text, $promoImage, $link, $id));
			}elseif($image != '' && $promoImage != ''){ 
			  $q->execute(array($name, $dev, $image, $promo_text, $promoImage, $link, $id));
			}else{
			  $q->execute(array($name, $dev, $promo_text, $link, $id));
			}
            
            Database::disconnect();
            header("Location: index.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM DailyDroidApp where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
		$name = $data['name'];
		$dev = $data['dev'];
        $image = $data['image'];
        $promo_text = $data['promo_text'];
		$promoImage = $data['promo_image'];
		$link = $data['link'];
		//$price = $data['price'];
		//$linkToShare = $data['linkToShare'];
        Database::disconnect();
    }
    ?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link   href="css/bootstrap.min.css" rel="stylesheet">
<script src="js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
<header class="header" style="text-align:center;">
<img src="img/logo.png" width="180" height="101" style="margin-top:20px;">
</header>

<div class="span10 offset1">
<div class="row">
<h3>Réviser cette App</h3>
</div>

<form enctype="multipart/form-data" class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">

<div class="control-group <?php echo !empty($nameError)?'error':'';?>">
<label class="control-label">Nom de l'application</label>
<div class="controls">
 <input name="name" type="text" placeholder="Nom d'application" value="<?php echo !empty($name)?$name:'';?>">
 <?php if (!empty($nameError)): ?>
 <span class="help-inline"><?php echo $nameError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="control-group <?php echo !empty($devError)?'error':'';?>">
<label class="control-label">Nom Developer</label>
<div class="controls">
 <input name="dev" type="text" placeholder="Nom developer" value="<?php echo !empty($dev)?$dev:'';?>">
 <?php if (!empty($devError)): ?>
 <span class="help-inline"><?php echo $devError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="control-group">
<label class="control-label">Icon</label>
<div class="controls">
<input name="image" type="file" id="image"> 
<?php echo '<img src="display_img.php?id='.$data['id'].'" width="100" height="100">';?>
</div>
</div>

<div class="control-group <?php echo !empty($promo_textError)?'error':'';?>">
<label class="control-label">Texte Promotional</label>
<div class="controls">
 <textarea name="promo_text" placeholder="Texte promo" cols="80" rows="6"><?php echo !empty($promo_text)?$promo_text:'';?></textarea>
 <?php if (!empty($promo_textError)): ?>
 <span class="help-inline"><?php echo $promo_textError;?></span>
 <?php endif; ?>
</div>
</div>

<div class="control-group">
<label class="control-label">Image promotional</label>
<div class="controls">
<input name="promo_image" type="file" id="promo_image"> 
<?php echo '<img src="display_promoimg.php?id='.$data['id'].'" width="100" height="auto">';?>
</div>
</div>

<div class="control-group <?php echo !empty($linkError)?'error':'';?>">
<label class="control-label">Lien</label>
<div class="controls">
 <input name="link" type="text" placeholder="Link d'application" value="<?php echo !empty($link)?$link:'';?>">
 <?php if (!empty($linkError)): ?>
 <span class="help-inline"><?php echo $linkError;?></span>
 <?php endif; ?>
</div>
</div>

<!--
<div class="control-group <?php //echo !empty($priceError)?'error':'';?>">
<label class="control-label">Prix</label>
<div class="controls">
 <input name="price" type="text" placeholder="Le prix" value="<?php //echo !empty($price)?$price:'';?>">
 <?php //if (!empty($priceError)): ?>
 <span class="help-inline"><?php //echo $priceError;?></span>
 <?php// endif; ?>
</div>
</div>

<div class="control-group <?php //echo !empty($linkToShareError)?'error':'';?>">
<label class="control-label">Lien pour le social network share</label>
<div class="controls">
 <input name="linkToShare" type="text" placeholder="Lien pour le social network" value="<?php //echo !empty($linkToShare)?$linkToShare:'';?>">
 <?php //if (!empty($linkToShareError)): ?>
 <span class="help-inline"><?php //echo $linkToShareError;?></span>
 <?php// endif; ?>
</div>
</div>
-->

<div class="form-actions">
<button type="submit" class="btn btn-success">Envoyer</button>
<a class="btn" href="index.php">Arrière</a>
</div>
</form>
</div>

</div> <!-- /container -->
</body>
</html>