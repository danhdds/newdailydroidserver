<?php   
/*
* @Author Danilo
* this script do the scarping given a url
* it extract the app's and store it in the database:
* Icon
* Name
* Dev
* Link
* Category
* Size
* Rating
* Main Image
* Video
*/
include '../database.php';

$array = array();

$pdo = Database::connect();

// take the latest record SELECT * FROM Table ORDER BY ID DESC LIMIT 1
$array = $pdo->query("SELECT `icon`, `name`, `dev`, `link`, `category`, `size`, `rating`, `image`, `video` 
FROM  `appOfDay`")->fetchAll(PDO::FETCH_ASSOC);

echo json_encode($array[0]);

Database::disconnect();

?>