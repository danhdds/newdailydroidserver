<?php   
/*
* @Author Danilo
* this script do the scarping given a url
* it extract the app's and store it in the database:
* Icon
* Name
* Dev
* Link
* Category
* Size
* Rating
* Main Image
* Video
*/
include '../database.php';

$array = array();
$url = 'https://play.google.com/store/apps/details?id=com.dotschallenge&hl=en';  
$icon = "";
$name = "";
$dev = "";
$link = "";
$category = "";
$size = "";
$rating  = "";
$image = "";
$video = "";

$pdo = Database::connect();

// take the latest record SELECT * FROM Table ORDER BY ID DESC LIMIT 1
$array = $pdo->query("SELECT * 
FROM  `appslist`")->fetchAll(PDO::FETCH_ASSOC);
//print_r($array);

foreach($array as $item){
   
   if($item['used'] == 0){
   
        $url = $item['LINKS'];   
		$id = $item['id'];
		$pdo->query("UPDATE `appslist` SET `used`=1 WHERE `id`= '$id'");		
        break; 
		
   } // end if
   
} // end foreach

$output = file_get_contents($url);  // store url in a variable
//echo $output;	
$dom = new DOMDocument; // create dom object

$dom->loadHTML($output); // load the url content on dom object

$finder = new DomXPath($dom); // here we create the finder 

$appList = array(); // create the array to store the apps information

/* Grab Main Icon image */

$src = $finder->evaluate("string(//img[@class='cover-image']/@src)"); // find the class

$icon = $src; 

/* grab title */
$classname="document-title"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
   $name = rtrim($node->nodeValue);
   $name = ltrim($name); 
} // end foreach

/* Grab Developers */
$classname="document-subtitle primary"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $dev = rtrim($node->nodeValue);
	$dev = ltrim($dev);
} // end foreach

/* store the app url */

$link = $url;

/* Grab App's category */
$classname="document-subtitle category"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $category = rtrim($node->nodeValue);
	$category = ltrim($category);
} // end foreach

/* Grab App's size */
$param="fileSize"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@itemprop, '$param')]"); // find the class

foreach ($nodes as $node) {
    $size = rtrim($node->nodeValue);
	$size = ltrim($size);
} // end foreach

/* Grab rating */
$classname="rating-box"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $string = str_replace(' ', '', $node->nodeValue);
    $rating = substr($string, 0, 3);
} // end foreach

/* Grab Ad Main image */

$src = $finder->evaluate("string(//img[@class='screenshot']/@src)"); // find the class

$image = $src;

/* Grab Video if exists */

$nodes = $finder->evaluate("string(//span[@class='id-track-click id-track-impression preview-overlay-container']/@data-video-url)"); // find the class

$video = $nodes;

$pdo->query("UPDATE `appOfDay` SET `icon`='$icon',`name`='$name',`dev`='$dev',`link`='$link',`category`='$category',`size`='$size',`rating`='$rating',`image`='$image',`video`='$video' WHERE `id`=1");

Database::disconnect();

?>