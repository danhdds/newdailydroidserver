<?php   
/*
* @Author Danilo
* this script do the scarping given a url
* it extract the app's:
* Icon
* Name
* Dev
* Link
* Category
* Size
* Rating
* Main Image
* Video
*/
header('Content-type: text/html; charset=utf-8; lang=fr');
//$url = 'https://play.google.com/store/apps/details?id=com.dailymotion.games&hl=en'; // Url to parse
$url = $_GET['url'];
$hl = $_GET['hl'];
if($hl) $url .= '&hl='.$hl; // to solve parameters issue in google links with &hl=en
//echo $url;

//header('Content-type: text/html; charset=utf-8 lang=fr');

$output = file_get_contents($url);  // store url in a variable
//echo $output;	
$dom = new DOMDocument; // create dom object

$dom->loadHTML($output); // load the url content on dom object

$finder = new DomXPath($dom); // here we create the finder 

$appList = array(); // create the array to store the apps information

/* Grab Main Icon image */

$src = $finder->evaluate("string(//img[@class='cover-image']/@src)"); // find the class

$appList['icon'] = $src; 

//echo $appList['appName'] . "<br>";

/* grab title */
$classname="document-title"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
   $appList['name'] = $node->nodeValue;
} // end foreach

//$appList['appTitle'] = $nodes->getChildren();

/* Grab Developers */
$classname="document-subtitle primary"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $appList['dev'] = $node->nodeValue; 
} // end foreach

/* store the app url */

$appList['link'] = $url;

/* Grab App's category */
$classname="document-subtitle category"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $appList['category'] = $node->nodeValue;
} // end foreach

/* Grab App's size */
$param="fileSize"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@itemprop, '$param')]"); // find the class

foreach ($nodes as $node) {
    $appList['size'] = $node->nodeValue;
} // end foreach

/* Grab rating */
$classname="rating-box"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $string = str_replace(' ', '', $node->nodeValue);
    $appList['rating'] = substr($string, 0, 3);
} // end foreach

//echo "<br>";
/* Grab Ad Main image */

$src = $finder->evaluate("string(//img[@class='screenshot']/@src)"); // find the class

$appList['image'] = $src;

/* Grab Video if exists */

$video = $finder->evaluate("string(//span[@class='preview-overlay-container']/@data-video-url)"); // find the class

$appList['video'] = $video;

/* Grab Description 
$classname="id-app-orig-desc"; // here we create the class that we want to grab its value

$nodes = $finder->query("//*[contains(@class, '$classname')]"); // find the class

foreach ($nodes as $node) {
    $appList['appDesc'] = $node->nodeValue;
} // end foreach
*/
echo json_encode($appList);

?>